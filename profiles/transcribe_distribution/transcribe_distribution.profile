<?php
/**
 * @file
 * Enables modules and site configuration for a site installation.
 */
 
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach($form['profile'] as $key => $element) {
    if($key != 'Transcribe Distribution') {
      $form['profile'][$key] = NULL; 
    }
  }
}

function transcribe_distribution_setup() {
  
  variable_set('theme_default', 'transcribe_theme');
  variable_set('admin_theme', 'seven');
  variable_set('node_admin_theme', 1);
  
  variable_set('pathauto_node_transcription_document_pattern', 'documents/[node:title]');
  variable_set('pathauto_node_transcription_page_pattern', 'documents/[node:transcription_document_ref]/pages/[node:title]');
  
  // set front page to our panel
  variable_set('site_frontpage', 'homepage');
  
  $vocab = taxonomy_vocabulary_machine_name_load('document_difficulty');
  $vid = $vocab->vid;
  if(!$vid) {
    watchdog(__FUNCTION__, 'Unable to load "document_difficulty" vocabulary');
  }
  //adding vocabularies.
  $difficulties = array('Beginner', 'Intermediate', 'Advanced');
  foreach($difficulties as $name) {
    $term = new stdClass();
    $term->name = $name;
    $term->vid = $vid;
    taxonomy_term_save($term);  
  }
}
