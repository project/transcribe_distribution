<?php
/**
 * @file
 * transcribe_features.default_corresponding_node_references_presets.inc
 */

/**
 * Implements hook_default_corresponding_node_references().
 */
function transcribe_features_default_corresponding_node_references() {
  $export = array();

  $cnr_obj = new stdClass();
  $cnr_obj->disabled = FALSE; /* Edit this to true to make a default cnr_obj disabled initially */
  $cnr_obj->api_version = 1;
  $cnr_obj->node_types_content_fields = 'transcription_page*transcription_document_ref*transcription_document*field_page_reference';
  $cnr_obj->enabled = 1;
  $export['transcription_page*transcription_document_ref*transcription_document*field_page_reference'] = $cnr_obj;

  return $export;
}
