<?php
/**
 * @file
 * transcribe_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function transcribe_features_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function transcribe_features_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function transcribe_features_node_info() {
  $items = array(
    'transcription_document' => array(
      'name' => t('Transcription document'),
      'base' => 'node_content',
      'description' => t('<em>Transcription documents</em> contain one or more <em>transcription pages</em>.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'transcription_page' => array(
      'name' => t('Transcription page'),
      'base' => 'node_content',
      'description' => t('<em>Transcription pages</em> are grouped into <em>transcription documents</em>.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
