<?php
/**
 * @file
 * transcribe_features.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function transcribe_features_taxonomy_default_vocabularies() {
  return array(
    'collection' => array(
      'name' => 'Collection',
      'machine_name' => 'collection',
      'description' => 'this acts as tags to categorize and highlight certain documents we want to feature',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'document_difficulty' => array(
      'name' => 'Difficulty',
      'machine_name' => 'document_difficulty',
      'description' => 'Difficulty of transcribing an uploaded document',
      'hierarchy' => '0',
      'module' => 'transcription',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
