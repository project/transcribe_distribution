<?php
/**
 * @file
 * transcribe_features.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function transcribe_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation:node/add/transcription-document
  $menu_links['navigation:node/add/transcription-document'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/transcription-document',
    'router_path' => 'node/add/transcription-document',
    'link_title' => 'Transcription document',
    'options' => array(
      'attributes' => array(
        'title' => '<em>Transcription documents</em> contain one or more <em>transcription pages</em>.',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/add',
  );
  // Exported menu link: navigation:node/add/transcription-page
  $menu_links['navigation:node/add/transcription-page'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/transcription-page',
    'router_path' => 'node/add/transcription-page',
    'link_title' => 'Transcription page',
    'options' => array(
      'attributes' => array(
        'title' => '<em>Transcription pages</em> are grouped into <em>transcription documents</em>.',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Transcription document');
  t('Transcription page');


  return $menu_links;
}
