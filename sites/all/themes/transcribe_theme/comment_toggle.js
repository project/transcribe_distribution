function transcription_tab_click() 
{
	var tt = document.getElementById('transcription-tab');
	var ct = document.getElementById('comment-tab');
	var td = document.getElementById('transcription-page-transcription');
	var cd = document.getElementById('transcription-page-comments');
	tt.className = 'active';
	ct.className = '';
	td.style.display = 'block';
	cd.style.display = 'none';
}

function comment_tab_click() 
{
	var tt = document.getElementById('transcription-tab');
	var ct = document.getElementById('comment-tab');
	var td = document.getElementById('transcription-page-transcription');
	var cd = document.getElementById('transcription-page-comments');
	tt.className = '';
	ct.className = 'active';
	td.style.display = 'none';
	cd.style.display = 'block';
}
