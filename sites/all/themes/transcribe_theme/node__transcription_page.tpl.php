<?php if($page): ?>
<?php
/**
 * Rendering full node
 */

$difficulty = taxonomy_term_load($document->document_difficulty[$document->language][0]['tid'])->name ; 
 
?>
<div id="transcription-page-header">
  <?php print render($title_prefix); ?>
 
  <h2 id="document-title" class=" <?php print $title_attributes; ?> "><span class="document-title-<?php print strtolower($difficulty); ?>"></span> <?php print $document->document_friendly_title[$document->language][0]['safe_value'] ?></h2>
  
  <?php print render($title_suffix); ?>
  
  <div id="transcription-page-subheader">
    
    <div id="document-pager">
      <?php print render($content['document_pager']); ?>
    </div>
    
    <div id="document-link">
      (
      National Archives Identifier
      <?php print l($document->document_link[$language][0]['title'], $document->document_link[$language][0]['url'], array('attributes'=>array('target'=>'_blank'))); ?>
      )
    </div>
    <div class="clearfix"></div>
  </div>

  
</div><!-- /#transcription-page-header -->
<div id="transcription-page-image">
  <?php print render($content['page_image']); ?>
</div>

<div id="transcription-page-middle" class="clearfix">
<!--Transcription status -->
  <div id="transcription-status-info">
    <?php if($completed) print '<div id="transcription-status">'. $completed. ' </div>'; ?>
    <?php
       $next_page = render($content['next_untranscribed_page']);
       if(strlen($next_page)) print '<span class="separator">/</span>'.$next_page;
       ?>
       

  </div>

<div id="transcription-page-image-resize">
<h4>Image Resize</h4>
 <div class="controls clearfix">
   <input id="zoom-in" class="zoom" type="submit" value="+" onclick="archives_scanned_image_zoom_in();return false;" />
   <input id="zoom-out" class="zoom" type="submit" value="-" onclick="archives_scanned_image_zoom_out();return false;" />
 </div>
</div><!-- /#transcription-page-image-resize -->
</div><!-- /#transcription-page-Middle -->



<div id="transcription-page-bottom" class="clearfix">
  <div id="transcription-page-tabs">
    <ul>
      <li id="transcription-tab" class="active" onclick="transcription_tab_click();">Transcription</li>
      <li id="comment-tab" class="" onclick="comment_tab_click();">Comments</li>
    </ul>
  </div><!-- /#transcription-page-tabs -->
  <div id="transcription-page-transcription" style="display: block;">
    <div id="transcription-page-transcription-content">
      <?php 
        $content['field_page_completed']['#title'] = "Transcription Status";
        print render($content['field_page_completed']);
      ?>
      <h3>What Do You See?</h3>
      <p>Please transcribe this National Archives document to improve its accessibility.</p>
      <h3>See a Mistake?</h3>
      <p>You can edit a previously transcribed page or resave a transcribed page as incomplete.</p> 
    </div>
    
   
    
   <div id="transcription-page-transcription-area">
    <?php if(user_access('update transcriptions')): ?>
    <?php hide($content['body']); ?>
    <?php hide($content['comments']); ?>
    <?php hide($content['links']); ?>
    <?php else: ?>
    <?php hide($content['transcribe_form']); ?>
    <?php hide($content['comments']); ?>
    <?php hide($content['links']); ?>
    <?php endif; ?>
    <?php if (transcribe_islocked($node->nid)) : ?>   
       <div class="lock">
         <h3 class="alert">This page is currently in use and locked. Please return later to add your transcription.</h3>
        <?php print render($content); ?>
       </div>
    <?php else : ?>
        <?php print render($content['transcribe_form']); ?>
    <?php endif; ?>
    </div>
    <div class="clearfix"></div>
  </div><!-- /#transcription-page-transcription -->
  <div id="transcription-page-comments" style="display: none;">
    
    
    <?php print render($content['comments']); ?>
  </div><!-- /#transcription-page-comments -->
  <!--Begin multi comment area -->
  
  <?php if ($content['comments']['comments'] && $node->type != 'forum'): ?>
    <?php print render($title_prefix); ?>
    <div class="clearfix" /></div>
    <h2 class="title"><?php print t('View Comments'); ?></h2>
    <?php print render($title_suffix); ?>
    
    <div id="multi-comment-wrapper">
  <?php print render($content['comments']['comments']); ?>
  </div>
    
  <?php endif; ?>
  
  <!--End multi comment area -->
</div><!-- /#transcription-page-bottom -->
<?php else: ?>
<?php
/**
 * Rendering list/teaser node
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

  
</div>
<?php endif; ?>
