/* Select the scanned image on the page */
function archives_select_scanned_image() 
{
	var e,x,y;
	e = document.getElementById('transcription-page-image');
	if(e) e = e.getElementsByTagName('img');
	if(e) {
		e = e[0];
		// Make sure we've got the width and height attributes set
		x = e.scrollWidth;
		y = e.scrollHeight;
		e.style.width = x+'px';
		e.style.height = y+'px';
		return e;
	}
	return false;
}

function archives_scanned_image_zoom_out() 
{
	var img, x, y;
	img = archives_select_scanned_image();
 	x = img.style.width.replace(/px$/,'');
 	y = img.style.height.replace(/px$/,'');
	x *= 3/4;
	y *= 3/4;
	img.style.width = x+'px';
	img.style.height = y+'px';
	return img;
}

function archives_scanned_image_zoom_in()
{
	var img, x, y;
	img = archives_select_scanned_image();
 	x = img.style.width.replace(/px$/,'');
 	y = img.style.height.replace(/px$/,'');
	x *= 4/3;
	y *= 4/3;
	img.style.width = x+'px';
	img.style.height = y+'px';
	return img;
}

