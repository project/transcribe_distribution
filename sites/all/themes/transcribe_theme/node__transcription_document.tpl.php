<?php if($page and false): ?>
<?php
/**
 * Rendering full node -- turned off for now;  node will always render as teaser until I see a mock design for it
 */

 ?>
<?php else: ?>
<?php
/**
 * Rendering list/teaser node
 */
// Set $url to point to the first page of the document
$url = url(drupal_get_path_alias('node/'.reset($node->total_pages)),array('absolute' => true));
// Load $difficulty for use in styling
$difficulty = taxonomy_term_load($node->document_difficulty[$node->language][0]['tid'])->name;
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <div class="document-image">
<?php
switch($view_mode) {
    // Here's where we change image style based on view mode
  case 'featured':
  // Use 'medium' style for 'featured' $view_mode
    $image_style = 'medium';
    break;
  default:
  // Use 'thumbnail' for all other $view_mode values
    $image_style = 'thumbnail';
}
    ?>  
    <?php
    
   
       $img = image_style_url($image_style, $node->document_image[$node->language][0]['uri']);
    if($img):
    ?>
    <a href="<?php print $url; ?>"><img src="<?php print $img; ?>" alt="<?php print $node->document_image[$node->language][0]['alt']; ?>" title="<?php print $node->document_image[$node->language][0]['title']; ?>" /></a>
    <?php endif; ?>
  </div> 
  <div class="document-info">
    <h3 class="document-title-<?php print strtolower($difficulty); ?>"><a href="<?php print $url; ?>"><?php print $node->document_friendly_title[$node->language][0]['safe_value']; ?></a></h3>
    <span class="document-date">
      <?php 
	 /**
	 * Note that we can't use the built-in date-formatters here, because they're all based on unix timestamp values,
	 * and we have to deal with values from before the epoch.  Instead, we'll fake the year afterward.
	 */
         $document_date = $node->document_date[$node->language][0]['value'];
      $fake_date = strtotime('2011'.substr($document_date,4));
      print date('F j, ', $fake_date).substr($document_date,0,4);
      ?>
    </span>
    <span class="document-difficulty document-difficulty-<?php print strtolower($difficulty); ?>">
      <?php print $difficulty; ?>
    </span>
    <span class="document-transcription-status">
      <?php print $node->transcribed_page_count; ?> / <?php print $node->total_page_count; ?> pages transcribed
    </span>
    <?php if($node->transcribed_page_count < $node->total_page_count): ?>
    <span class="document-transcribe-link">
      <?php
         $untranscribed_nids = array_diff($node->total_pages, $node->transcribed_pages);
      print l(t('Transcribe'), url(drupal_get_path_alias('node/'.reset($untranscribed_nids))), array('absolute' => true));
      ?>
    </span>
    <?php endif; ?>
  </div>
  <div class="clearfix"></div>
</div>
<?php endif; ?>
