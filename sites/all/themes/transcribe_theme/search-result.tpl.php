<?php
  /**
   * Custom search result formatting for nodes
   */
?>
<?php
if(array_key_exists('node',$variables['result']) and $variables['result']['node']->type == 'transcription_document'):
/**
 * transcription_document node type
 */
// Load the node so we get our handy hooks into it
$node = node_load($variables['result']['node']->nid);
// Update $url to point to the first page of the document
$url = url(drupal_get_path_alias('node/'.reset($node->total_pages)),array('absolute' => true));
?>
<li class="<?php print $classes; ?> search-result-transcription-document"<?php print $attributes; ?>>
  <div class="document-image">
    <?php
       $img = image_style_url('thumbnail', $node->document_image[$node->language][0]['uri']);
    if($img):
    ?>
    <a href="<?php print $url; ?>"><img src="<?php print $img; ?>" alt="<?php print $node->document_image[$node->language][0]['alt']; ?>" title="<?php print $node->document_image[$node->language][0]['title']; ?>" /></a>
    <?php endif; ?>
  </div>
  <div class="document-info">
    <h3><a href="<?php print $url; ?>"><?php print $node->document_friendly_title[$node->language][0]['safe_value']; ?></a></h3>
    <span class="document-date">
      <?php 
	 /**
      * Note that we can't use the built-in date-formatters here, because they're all based on unix timestamp values,
      * and we have to deal with values from before the epoch.  Instead, we'll fake the year afterward.
      */
        $document_date = $node->document_date[$node->language][0]['value'];
        $fake_date = strtotime('2011'.substr($document_date,4));
        print date('F j,', $fake_date).substr($document_date,0,4);
	 ?>
    </span>
    <?php $difficulty = $node->document_difficulty[$node->language][0]['taxonomy_term']->name; ?>
    <span class="document-difficulty document-difficulty-<?php print $difficulty; ?>">
      Difficulty: <?php print $difficulty; ?>
    </span>
    <span class="document-transcription-status">
      <?php print $node->transcribed_page_count; ?> / <?php print $node->total_page_count; ?> pages transcribed
    </span>
    <?php if($node->transcribed_page_count < $node->total_page_count): ?>
    <span class="document-transcribe-link">
         <?php
         $untranscribed_nids = array_diff($node->total_pages, $node->transcribed_pages);
         print l(t('Transcribe'), url(drupal_get_path_alias('node/'.reset($untranscribed_nids))), array('absolute' => true));
         ?>
    </span>
    <?php endif; ?>
  </div>
</li>
<?php
elseif(array_key_exists('node',$variables['result']) and $variables['result']['node']->type == 'transcription_page'):
/**
 * transcription_page node type
 */
// Load the node so we get our handy hooks into it
$node = node_load($variables['result']['node']->nid);
$document = node_load($node->document->nid);
$current_page = array_search($node->nid, $document->total_pages)+1;
// Update $url to point to the relevant page
$url = url(drupal_get_path_alias('node/'.$node->nid),array('absolute' => true));
?>
<li class="<?php print $classes; ?> search-result-transcription-page"<?php print $attributes; ?>>
  <div class="page-image">
    <?php
       $img = image_style_url('thumbnail', $node->page_image[$node->language][0]['uri']);
    if($img):
    ?>
    <a href="<?php print $url; ?>"><img src="<?php print $img; ?>" alt="<?php print $node->page_image[$node->language][0]['alt']; ?>" title="<?php print $node->page_image[$node->language][0]['title']; ?>" border="0"></a>
    <?php endif; ?>
  </div>
  <div class="page-info">
    <h3><a href="<?php print $url; ?>"><?php print $document->document_friendly_title[$document->language][0]['safe_value']; ?></a></h3>
    <h4>Page <?php print $current_page; ?> of <?php print $document->total_page_count; ?></h4>
    <span class="document-date">
      <?php 
	 /**
      * Note that we can't use the built-in date-formatters here, because they're all based on unix timestamp values,
      * and we have to deal with values from before the epoch.  Instead, we'll fake the year afterward.
      */
        $document_date = $document->document_date[$document->language][0]['value'];
        $fake_date = strtotime('2011'.substr($document_date,4));
        print date('F j,', $fake_date).substr($document_date,0,4);
	 ?>
    </span>
    <?php $difficulty = $document->document_difficulty[$document->language][0]['taxonomy_term']->name; ?>
    <span class="document-difficulty document-difficulty-<?php print $difficulty; ?>">
      Difficulty: <?php print $difficulty; ?>
    </span>
    <span class="document-transcription-status">
      <?php print $document->transcribed_page_count; ?> / <?php print $document->total_page_count; ?> pages transcribed
    </span>
    <?php if($document->transcribed_page_count < $document->total_page_count): ?>
    <span class="document-transcribe-link">
         <?php
         $untranscribed_nids = array_diff($document->total_pages, $document->transcribed_pages);
         print l(t('Transcribe'), url(drupal_get_path_alias('node/'.reset($untranscribed_nids))), array('absolute' => true));
         ?>
    </span>
    <?php endif; ?>
  </div>
</li>
<?php else: ?>    
<?php
/**
 * Default search result
 */
?>      
<li class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="title"<?php print $title_attributes; ?>>
    <a href="<?php print $url; ?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info">
    <?php if ($snippet) : ?>
      <p class="search-snippet"<?php print $content_attributes; ?>><?php print $snippet; ?></p>
    <?php endif; ?>
    <?php if ($info) : ?>
      <p class="search-info"><?php print $info; ?></p>
    <?php endif; ?>
  </div>
</li>
<?php endif; ?>
