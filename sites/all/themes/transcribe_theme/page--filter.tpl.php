<div id="page-wrapper">
  <div id="page">
    <div id="header">
      <div class="section clearfix">
	
	<?php if ($logo): ?>
       <a href="http://www.archives.gov/" title="National Archives" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
	<?php endif; ?>
	
	<?php if ($site_name): ?>
        <div id="header-site-name">
          <?php if ($site_name): ?>
          <div id="site-name"><strong>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </strong></div>
          <?php endif; ?>
        </div> <!-- /#site-name -->
	<?php endif; ?>

	<?php if($main_menu): ?>
	<div id="header-main-menu">
          <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => false)); ?>
	</div> <!-- /#main-menu -->
	<?php endif; ?>

	<?php if ($secondary_menu): ?>
	<div id="header-secondary-menu">
	  <div class="section">
            <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => false)); ?>
	  </div> <!-- /#section -->
	</div> <!-- /#secondary-menu -->
	<?php endif; ?>

	<?php print render($page['header']); ?>

      </div> <!-- /#section -->
    </div> <!-- /#header -->

    <?php print $messages; ?>

    <div id="main-wrapper"><div id="main" class="clearfix">

      <div id="content" class="column"><div class="section">
       
     

        <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <h1 class="title" id="page-title">Text Formatting Tips</h1>
       
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div></div> <!-- /.section, /#content -->

    </div></div> <!-- /#main, /#main-wrapper -->

    <div id="footer"><div class="section">
      <?php print render($page['footer']); ?>
    </div></div> <!-- /.section, /#footer -->

  </div></div> <!-- /#page, /#page-wrapper -->
