<?php
/**
 * Implementation of hook_theme().
 * We're using it here to override the template files from transcribe_theme_preprocess_page() and transcribe_theme_preprocess_node()
 */
function transcribe_theme_theme($existing, $type, $theme, $path) {
  return array(
    'node__transcription_document' => array(
      'template' => 'node__transcription_document',
    ),
    'page__node__transcription_page' => array(
      'template' => 'page__node__transcription_page',
    ),
    'node__transcription_page' => array(
      'template' => 'node__transcription_page',
    ),

  );
}

/**
 * Implementation of hook_preprocess_page()
 */
function transcribe_theme_preprocess_page(&$variables, $hook) {
  if (isset($variables['node']->type)) {
    $variables['theme_hook_suggestions'][] = 'page__node__'.$variables['node']->type;
      
    /* Locking Feature. Unlock everything was locked if you browse away from the page. */
    if (isset($_SESSION[TRANSCRIBE_LAST_NODE_LOCKED]) && $variables['node']->type!='transcription_page') {
      transcribe_unlock($_SESSION[TRANSCRIBE_LAST_NODE_LOCKED], true);
      unset($_SESSION[TRANSCRIBE_LAST_NODE_LOCKED]);
    }
  }
  if (isset($variables['main_menu']) && isset($variables['menu-1916'])) {
    $copy = $variables['main_menu']['menu-1916']['title'];
    $stringBrowser = str_replace(' ', '&nbsp;', $copy);
    $tags = '&nbsp;';
    $variables['main_menu']['menu-1916']['title'] = drupal_html_to_text($stringBrowser, $tags);
  }
  
  //Add stylesheet for IE7
  drupal_add_css(
    drupal_get_path('theme', 'transcribe_theme') . '/ie7.css',
    array(
      'group' => CSS_THEME,
      'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE),
      'preprocess' => FALSE,
    )
  );
}

/**
 * Implementation of hook_preprocess_node()
 */
function transcribe_theme_preprocess_node(&$variables, $hook) {
  if (isset($variables['node']->type)) {
    $variables['theme_hook_suggestions'][] = 'node__'.$variables['node']->type;
  }
  
  if(isset($variables['field_page_completed'])){
    $variables['completed'] = '';
    $completed = "";
    $status = $variables['field_page_completed'][0]['value'];
        
    switch($status){
      case '0':
        $completed = 'This page has not been transcribed';
        break;
      case '1':
        $completed = 'This page has been transcribed';
        break;
      case '2':
        $completed = 'This page is partially transcribed';
        break;    
    }
    
    $variables['completed'] = $completed;
  }
}

/**
 * Custom theme for main menu links;  removing spaces
 */
function transcribe_theme_links__system_main_menu($variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    $output = '';

    // Treat the heading first if it is present to prepend it to the list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading. 
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
           && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>";
    }

    $output .= '</ul>';
  }

  return $output;
} 

/**
 * Custom theme for secondary links;  uses pipes as separators
 */
function transcribe_theme_links__system_secondary_menu($variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    $output = '<div' . drupal_attributes($attributes) . '>';
    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
           && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<span' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }
      $output .= "</span>\n";
      if($i < $num_links) $output .= '<span class="separator">|</span>';
      $i++;
    }

    $output .= '</div>';
  }

  return $output;
}

/**
 * Custom theme for footer menu tree
 */
function transcribe_theme_menu_tree__menu_footer_menu($variables){
  return '<div class="footer-menu">' . $variables['tree'] . '</div>';
}

/**
 * Custom theme for footer menu links;  uses pipes as separators
 */
function transcribe_theme_menu_link__menu_footer_menu($variables){
  $output = '';
  $element = $variables['element'];
  $sub_menu = '';
  
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  $output = '<span' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</span>\n";

  if(array_key_exists('class', $element['#attributes']) and !in_array('last', $element['#attributes']['class'])) {
    $output .= '<span class="separator">|</span>';
  }
  return $output;
}

function transcribe_theme_preprocess_comment(&$variables) {
  $comment = $variables['elements']['#comment']->created;
  $variables['date_month'] = format_date($comment, 'custom', 'F');
  $variables['date_day'] = format_date($comment, 'custom', 'j');
  $variables['date_year'] = format_date($comment, 'custom', 'Y');
}

function transcribe_theme_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
  $form['difficulty']['#options']['All'] = 'Select difficulty';
  $form['field_pages_completed_value']['#options']['All'] = 'Transcription Status';
  $form['year']['#options']['All'] = 'Select Year';
}

function transcribe_theme_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#form_id'] == 'search_block_form') {   
    $button_path = drupal_get_path('theme', 'transcribe_theme');  
    $form['actions']['submit']['#value'] = 'GO';
    $form['actions']['submit']['#alt'] = 'Go';
    $form['actions']['submit']['#title_display'] = 'invisible';
    $form['actions']['submit']['#title'] = 'Go';
  }
  
  if ($form['#form_id'] == 'transcription_form') {
    $form['submit']['incomplete']['#title'] ='Save as Incomplete';
    $form['submit']['incomplete']['#title_display'] ='invisible';
    $form['submit']['complete']['#title'] ='Save as Complete';
    $form['submit']['complete']['#title_display'] ='invisible';
    $form['transcription']['#title'] ='Transcription Text Area';
    $form['transcription']['#title_display'] ='invisible';
  }
  if ($form['#form_id'] ==  'comment_node_transcription_page_form') {
    //TODO
  }
}

function transcribe_theme_preprocess_field(&$vars, $hook) {
  if($vars['element']['#field_name'] == 'field_page_completed') {
    $vars['classes_array'][] = preg_replace('/[^a-z0-1]+/', '-', strtolower($vars['element'][0]['#markup']));
  }
}

function transcribe_theme_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.
 
  $li_first = theme('pager_first', array('text' =>  t('first'), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('Prev')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('Next')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '...',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '...',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}
